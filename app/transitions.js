export default function(){
  this.transition(
    this.fromRoute('index'),
    this.toRoute('realms'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
}