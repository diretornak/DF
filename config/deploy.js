
/* jshint node: true */

var gcsServiceAccountCredsPath = 'gcs-service-account-creds';
var gcsServiceAccountCreds = require('./' + gcsServiceAccountCredsPath);

module.exports = function(deployTarget) {
  var ENV = {
    build: {
      environment: deployTarget,
    },
    'revision-data': {
      type: 'git-commit'
    },
  };
  var bucket = '';

  ENV['gcloud-storage'] = {
    credentials: {
      "private_key": gcsServiceAccountCreds.private_key,
      "client_email": gcsServiceAccountCreds.client_email,
    },
    projectId: '<your-project-id>',
  };

  ENV['gcs-index'] = {
    allowOverwrite: true,
    projectId: '<your-project-id>',
    keyFilename: 'config/' + gcsServiceAccountCredsPath + '.json',
  };

  if (deployTarget === 'development') {
  }

  if (deployTarget === 'staging') {
    bucket = '<your-bucket-name>';

    ENV.prependToFingerprint = 'https://storage.googleapis.com/' + bucket + '/';
    ENV['gcloud-storage'].bucket = bucket;
    ENV['gcs-index'].bucket = bucket;
  }

  if (deployTarget === 'production') {
    bucket = '<your-bucket-name>';

    ENV.prependToFingerprint = 'https://storage.googleapis.com/' + bucket + '/';
    ENV['gcloud-storage'].bucket = bucket;
    ENV['gcs-index'].bucket = bucket;
  }

  return ENV;
};
view rawdeploy.js hosted with ❤ by GitHub